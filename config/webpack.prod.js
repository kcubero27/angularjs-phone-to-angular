module.exports = {
  module: {
    loaders: [
      {
        test: /\.css$/,
        loaders: ['to-string-loader', 'css-loader', 'postcss-loader']
      },
      {
        test: /\.(sass|scss)$/,
        loaders: ['to-string-loader', 'css-loader', 'postcss-loader', 'resolve-url-loader', 'sass-loader']
      },
    ]
  }
};
