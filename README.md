# AngularMigration

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6.

## Requirements
Install NodeJS
Install AngularCLI
Install http-server

## Development server

Run `npm run serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build. To run the server, go inside `dist/{project_name}` and run `http-server`.

Run `npm run build-storybook` to build the storybook. The build artifacts will be stored in the `storybook-static/` directory. To run the server, go inside `storybook-static/` and run `http-server`.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Pending tasks
- [x] Angular 7
- [ ] Docker
- [ ] Makefile
- [x] TSLint
- [ ] Prettier
- [ ] StyleLint
- [ ] StyleLint linter
- [ ] App Example
- [x] Storybook
- [x] SCSS
- [x] PostCSS
