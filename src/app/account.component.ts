import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-account',
  template: `
    Bank Name: {{bankName}}
    Account Id: {{accountId}}
  `
})

export class AccountComponent {
  @Input() bankName: string;
  @Input() accountId: string;
}
