import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {UrlHandlingStrategy} from '@angular/router';
import {DashboardComponent} from './dashboard.component';
import {SettingsComponent} from './settings.component';
import {UpgradeModule} from '@angular/upgrade/static';

export class CustomHandlingStrategy implements UrlHandlingStrategy {
  shouldProcessUrl(url) {
    return url.toString().startsWith('/ng2-route') || url.toString() === '/';
  }

  extract(url) {
    return url;
  }

  merge(url, whole) {
    return url;
  }
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    UpgradeModule,
    AppRoutingModule
  ],
  providers: [
    {provide: UrlHandlingStrategy, useClass: CustomHandlingStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
